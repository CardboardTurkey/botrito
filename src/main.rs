use anyhow::Result;
use tracing::Level;

use irc_handle::connect_irc;
use train::Controller;

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt()
        .with_max_level(Level::DEBUG)
        .init();

    let controller: Controller = Default::default();
    connect_irc("config.toml".to_string(), controller).await?;

    Ok(())
}
