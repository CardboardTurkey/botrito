use futures_util::StreamExt;
use irc::client::prelude::*;
use std::env;
use tokio::sync::mpsc;
use tracing::{debug, info, warn};

use train::Bot;

struct IrcActor<B: Bot> {
    receiver: mpsc::Receiver<String>,
    transmitter: mpsc::Sender<String>,
    bot: B,
    config: Config,
}

impl<B: Bot> IrcActor<B> {
    fn new(
        receiver: mpsc::Receiver<String>,
        transmitter: mpsc::Sender<String>,
        bot: B,
        config: Config,
    ) -> Self {
        Self {
            receiver,
            transmitter,
            bot,
            config,
        }
    }
}

async fn run_irc_actor<B: Bot>(mut actor: IrcActor<B>) {
    // let mut client = Client::from_config(config).await?;
    let mut client = Client::from_config(actor.config).await.unwrap();
    client.identify().unwrap();
    let mut stream = client.stream().unwrap();
    info!("Connected to IRC server as {}", client.current_nickname());

    let mut chan = "".to_string();
    loop {
        tokio::select! {
            opt_msg = stream.next() => {
                match opt_msg.transpose().unwrap() {
                    None => break,
                    Some(message) => {
                        if let Command::PRIVMSG( ref channel, ref msg) = message.command {
                            if msg.starts_with(client.current_nickname()) {
                                info!("New message addressed to bot");
                                debug!("{}", msg);
                                if let Some(sender) = message.source_nickname() {
                                    if actor.bot.handle_message(sender.to_string(), msg.to_string(), actor.transmitter.clone()).is_ok() {
                                        info!("Message succesfully handled. Message: {:?}", msg);
                                        let _ = client.send_privmsg(channel, format!("{}: Ok", sender));
                                        chan = channel.clone();
                                    } else {
                                        warn!("Failed to process message {}", msg);
                                        let _ = client.send_privmsg(channel, "Something went wrong try again ヽ(。_°)ノ");
                                    };
                                };
                            }
                        }
                    }
                };
            },
            Some(msg) = actor.receiver.recv() => {
                client.send_privmsg(&chan, &msg).unwrap();
            },
        }
    }
}

fn env_var_config() -> Config {
    Config {
        server: env::var("IRC_SERVER").ok(),
        port: env::var("IRC_PORT")
            .map(|s| s.parse().expect("IRC_PORT is not a u16!"))
            .ok(),
        nickname: env::var("IRC_NICKNAME").ok(),
        username: env::var("IRC_USERNAME").ok(),
        password: env::var("IRC_PASSWORD").ok(),
        use_tls: env::var("IRC_USE_TLS")
            .map(|s| s.parse().expect("IRC_USE_TLS is not a bool!"))
            .ok(),
        channels: env::var("IRC_CHANNELS")
            .expect("IRC_CHANNELS must be defined")
            .split(',')
            .map(String::from)
            .collect(),
        user_info: env::var("IRC_USER_INFO").ok(),
        ..Config::default()
    }
}

pub fn connect_irc<B>(config_path: String, bot: B) -> tokio::task::JoinHandle<()>
where
    B: Bot + std::marker::Send + 'static,
{
    let (transmitter, receiver) = mpsc::channel(16);
    let config = if std::path::Path::new(&config_path).exists() {
        Config::load(config_path).expect("Failed to load config from file")
    } else {
        env_var_config()
    };
    let actor = IrcActor::new(receiver, transmitter, bot, config);
    let handle = tokio::spawn(run_irc_actor(actor));
    info!("Connected to IRC");
    handle
}
