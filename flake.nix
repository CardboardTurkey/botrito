{
  description = "Burrito Train Bot";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
    flake-utils.url  = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
    naersk.url = "github:nmattia/naersk/master";
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay, naersk }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs { inherit system overlays; };
        naersk-lib = naersk.lib."${system}".override {
          cargo = pkgs.rust-bin.stable.latest.default;
          rustc = pkgs.rust-bin.stable.latest.default;
        };

      name = "botrito";
      in rec {
        packages = {
          default = packages.${name};
          ${name} = naersk-lib.buildPackage {
            pname = name;
            root = ./.;
            nativeBuildInputs = with pkgs; [
              openssl
              pkg-config
              rust-bin.stable.latest.default
            ];
            doCheck = true;
          };
          docker = let
              app = packages.default;
            in pkgs.dockerTools.buildLayeredImage {
            name = app.pname;
            tag = app.version;
            contents = [ app ];

            config = {
              Cmd = ["/bin/${name}"];
              WorkingDir = "/";
            };
          };
        };

        apps = {
          default = apps.${name};
          ${name} = flake-utils.lib.mkApp {
            drv = packages.${name};
          };
        };

        devShells = {
          default = devShells.${name};
          ${name} = with pkgs; mkShell {
            buildInputs = [
              openssl
              pkg-config
              rust-bin.stable.latest.default
            ];
          };
        };
      }
    );
}
